// 3
// fetch("https://jsonplaceholder.typicode.com/todos")
// .then((response) => response.json())
// .then((json) => console.log(json));


// 4
// fetch("https://jsonplaceholder.typicode.com/todos")
// .then(response => response.json())
// .then(data => {
//   let dataVar = data.map((elem) => {
//     return(elem.title)
//   }); 
  
//   console.log(dataVar);
// });



// 5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then((json) => console.log(json)
);



// 6
fetch("https://jsonplaceholder.typicode.com/todos/")
.then(response => response.json())
.then(data => {
  let title = data.map((elem) => {
    return (elem.title)
  });
  let status = data.map((elem) => {
    return (elem.completed)
  });

  console.log(`The item \"${title[0]}\" on the list has a status of ${status[0]}`);
});



// 7 
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	
	headers: {
		"Content-type": "application/json",
	},
	
	body: JSON.stringify({
    completed: false,
		title: "Created To Do List Item",
    userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// 8 - 9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	
	headers: {
		"Content-type": "application/json",
	},
	
	body: JSON.stringify({
    dateCompleted: "Pending",
    description: "To update the my to do list with a different data structur...",
    status: "Pending",
		title: "Update To Do List Item",
    userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));




// 10

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	
	headers: {
		"Content-type": "application/json",
	},
	
	body: JSON.stringify({
    completed: false,
    dateCompleted: "07/09/21",
    status: "Complete",
		title: "delectus aut autem",
    userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// 12

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
});